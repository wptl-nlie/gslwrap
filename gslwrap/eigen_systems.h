#ifndef __eigen_systems_h
#define __eigen_systems_h

#include "gslwrap/matrix_double.h"
#include "gsl/gsl_eigen.h"

namespace gsl
{

//! Calculates eigenvalues and eigenvectors of a symmetric matrix. The vectors and values are ordered so that the eigenvalues will be in descending order
class real_eigenvectors
{
 public:
	template <class MatrixType>
	real_eigenvectors(const MatrixType& covariancematrix) : 
		eigenvectors(covariancematrix.get_rows(), covariancematrix.get_cols()),
		eigenvalues(covariancematrix.get_rows())
		{
			gsl_eigen_symmv_workspace * work = gsl_eigen_symmv_alloc (covariancematrix.get_rows());
			matrix s;
			s = covariancematrix;
			gsl_eigen_symmv (s.gslobj(), eigenvalues.gslobj(), eigenvectors.gslobj(), work) ;
			gsl_eigen_symmv_free (work);
			gsl_eigen_symmv_sort (eigenvalues.gslobj(), eigenvectors.gslobj(), GSL_EIGEN_SORT_VAL_DESC) ;
		}
	matrix eigenvectors;
	vector eigenvalues;
 private:
};
}
#endif //__eigen_systems_h
